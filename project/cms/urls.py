from django.urls import path
from . import views
urlpatterns = [
    path("", views.default_view),
    path("<str:llave>", views.get_content),
]
