from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Contents
from django.shortcuts import get_object_or_404

FORM = """
<p>
Por favor, introduce un valor:
<p>
<form action="" method="POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" name="Enviar">
</form>
"""


@csrf_exempt
def default_view(request):
    return HttpResponse("¡Bienvenido a la aplicación CMS PUT!")

@csrf_exempt
def get_content(request, llave):
    if request.method == "POST":
        valor = request.POST.get('valor', '')
        if valor:
            c, created = Contents.objects.get_or_create(clave=llave, defaults={'valor': valor})
            if not created:
                c.valor = valor
                c.save()
    elif request.method == "PUT":
        valor = request.body.decode('utf-8')
        c, _ = Contents.objects.update_or_create(clave=llave, defaults={'valor': valor})

    contenido = Contents.objects.filter(clave=llave).first()  # Verificar si el objeto existe
    if contenido:
        return HttpResponse(contenido.valor)
    else:
        return HttpResponse(FORM)
